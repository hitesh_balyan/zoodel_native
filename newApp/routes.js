import React from 'react';
import {StackNavigator, TabNavigator} from 'react-navigation';
import {Button, Icon} from 'native-base';

import HomeScreen from './screens/home-screen';
import MallScreen from './screens/mall-screen';
import DealScreen from './screens/deal-screen';
import ItemDetailScreen from './screens/item-details-screen';
import CartScreen from "./screens/cart-screen";
import SettingScreen from "./screens/account";



import {COLORS} from "./constants";


const baseNavigator = TabNavigator({
  home: {
    screen: HomeScreen,
    navigationOptions: {
      tabBarIcon: ({tintColor, focused}) => (
          <Icon style={{color: tintColor, fontSize: 20}} name="ios-home-outline"/>)
    }
  },
  mallMain: {
    screen: MallScreen,
    navigationOptions: {
      tabBarIcon: ({tintColor, focused}) => (
          <Icon style={{color: tintColor, fontSize: 20}} name="apps"/>)
    }
  },
  dealMain: {
    screen: DealScreen,
    navigationOptions: {
      tabBarIcon: ({tintColor, focused}) => (
          <Icon style={{color: tintColor, fontSize: 20}} name="ios-pricetag-outline"/>)
    }
  },
  cartMain: {
    screen: CartScreen,
    navigationOptions: {
      tabBarIcon: ({tintColor, focused}) => (
          <Icon style={{color: tintColor, fontSize: 20}} name="ios-cart-outline"/>)
    }
  },
  userMain: {
    screen: SettingScreen,
    navigationOptions: {
      tabBarIcon: ({tintColor, focused}) => (
          <Icon style={{color: tintColor, fontSize: 20}} name="person"/>)
    }
  },
}, {
  tabBarOptions: {
    activeTintColor: COLORS.active,
    inactiveTintColor: COLORS.inactive,
    initialRouteName: 'home',
    showIcon: true,
    showLabel: false,

    style: {
      backgroundColor: COLORS.white,
      borderColor: COLORS.lighBG,
      borderTopWidth: 2,
      height: 42,
    },
    tabStyle: {
      padding: 5,
    }
  },
  initialRouteName: 'home',
  lazy: true,
  animationEnabled: true,
  swipeEnabled: false,
  tabBarPosition: 'bottom',
  backBehavior: 'none'
});

const bootstrap = StackNavigator({
  Root: {
    screen: baseNavigator,
    navigationOptions: {header: null}
  },
  itemDetail: {
    screen: ItemDetailScreen,
    path: 'item/:title',
    navigationOptions: ({navigation}) => {
      return {title: `${navigation.state.params.title}!`};
    },
  },
  paymentConfirmation: {
    screen: ItemDetailScreen,
    path: 'paymentConfirm/:title',
    navigationOptions: ({navigation}) => {

    }
  },
});

export default bootstrap;