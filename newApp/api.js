import base64 from 'base-64';

const URI = 'https://sandboxwww.zoodmall.com/interface/h5.php';
const COMMON_HEADER = new Headers({
  "Accept-Encoding": "gzip, deflate",
  "Accept-Language": "en-IN,en-US;q=0.9",
  "User-Agent": "Mozilla/5.0 (Linux; U; Android 7.0; ZoodMall Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.146 Mobile Safari/537.36 ZoodMall/ZoodMallBrowser/8.7.8",
  "Content-Type": "application/x-www-form-urlencoded",
  "X-Requested-With": "XMLHttpRequest"
});


const json = (response) => (response.json());
const status = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response)
  } else {
    return Promise.reject(new Error(response.statusText))
  }
};
const mergerReset = (data) => {
  for (key in data) {
    switch (key) {
      case 'sKillPrice':
      case 'bonus':
        data['price'] = data[key];
        delete data[key];
        break;
      case 'img':
        data['imgUrl'] = data[key];
        delete data[key];
        break;
    }
  }
  return Object.assign({}, {
    "sKillId": null,
    "crossedPrice": null,
    "savePrice": null,
    "period": null,
    "expiredTime": null,
    "status": null,
    "productId": null,
    "sponsorShopId": null,
    "discount": null,
    "name": null,
    "soldCnt": 0,
    "categoryId": null,
    "sponsorProductId": null,
    "tagValue": null,
    "price": 0,
    "imgUrl": '',
    "isShareEarn": null,
    "timeShareExpireSecond": null
  }, data);
};
const caller = (api, method = 'GET', opts = {}) => {
  let options = Object.assign({}, {
    "page": 1,
    "sandbox": true,
    "lang": "en",
    "ver": "00.00.03",
    "deviceInfo": "Xiaomi(Device:Redmi 3S_OS:6.0.1)",
    "marketCode": "KZ"
  }, opts);
  let encodeParams = base64.encode(JSON.stringify(options));
  let fetchRequest = {
    method: method,
    headers: COMMON_HEADER,
    body: `api=${api}&rand=0.1550762187512067&encodeParams=${encodeParams}`
  };
  return fetch(URI, fetchRequest).catch(function (error) {
    console.error(error);
  }).then(status).then(json);
};


const GetProductDetails = (args, callback) => {
  let {productId, customerId, sponsorShopId} = args;
  let fetcher = caller(
      'product.videodetail',
      'POST',
      {
        "productId": productId,
        "customerId": customerId,
        "sponsorShopId": sponsorShopId,
        "hotRankArr": 1,
      }
  );
  fetcher.then(function (data) {
    if (data.Data !== void 0) {
      callback(null, data.Data.Product);
    }
  });
};

const GetListBy = (callback, page = 1, opts = {}) => {
  let fetcher = caller(
      'product.productlist',
      'POST',
      Object.assign({}, {
        page: page
      }, opts)
  );
  fetcher.then(function (data) {
    if (data.Data !== void 0) {
      callback(null, data.Data.marketList.map(mergerReset));
    }
  });
};

const GetDeals = (callback, page = 1, opts = {}) => {
  let fetcher = caller(
      'product.seckillproductlist',
      'POST',
      Object.assign({}, {
        "isSkillProduct": 1,
        page: page
      }, opts)
  );

  fetcher.then(function (data) {
    if (data.Data !== void 0) {
      callback(null, data.Data.data.map(mergerReset));
    }
  });
};


export {GetProductDetails, GetListBy, GetDeals,};