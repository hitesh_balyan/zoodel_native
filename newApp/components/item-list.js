import React, {Component} from 'react';
import {ActivityIndicator, FlatList, RefreshControl} from 'react-native';

import {ItemThumbnail} from "./item-thumbnail";
import {COLORS} from "../constants";

export class ItemList extends Component {
  _rowRefs = [];
  setVisibility = (visibility) => {
    if (!visibility) {
      this.setState({data: [], page: 1, refreshing: false});
    } else {
      this._loadData();
    }
  };
  _addRef = (index, item) => {
    this._rowRefs[index] = item;
  };
  _itemRenderer = ({item, index}) => {

    let priceObj = {
      current: parseFloat(item.price),
      original: 0,
      isSaved: false,
      savePresent: 0
    };
    if (item.sKillId) {
      priceObj.original = parseFloat(item.crossedPrice);
      priceObj.savePresent = parseFloat(item.savePrice);
      priceObj.isSaved = true;
    }
    return <ItemThumbnail
        ref={item => {
          this._addRef(index, item)
        }}
        uri={item.imgUrl}
        price={priceObj}
        name={item.name}
        sold={parseInt(item.soldCnt)}
        onPress={() => {
          console.log(item);
          this.props.navigation.navigate('itemDetail', {title: item.name, data: item});
        }}
    />;
  };
  _onRefreshHandler = () => {
    this.setState({refreshing: true}, this._loadData);
  };
  _onEndHandler = ({distanceFromEnd}) => {
    if (distanceFromEnd <= 147) {
      let page = this.state.page + 1;
      this.setState({
        refreshing: true,
        page: page
      }, this._loadData);
    }
  };
  _loadData = () => {
    this._props.dataHandler((err, data) => {
      let _data = this.state.data;
      _data = _data.concat(data);
      this.setState({data: _data, refreshing: false});
    }, this.state.page);
  };
  _onViewableItemsChanged = (info) => {
    info.changed.map(item => {
      this._rowRefs[item.index].setVisibility(item.isViewable);
      return item.isViewable;
    })
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      refreshing: true,
      page: 1
    };
    this._props = {dataHandler: props.dataHandler};
    this._onViewableItemsChanged = this._onViewableItemsChanged.bind(this);
    this.onEndReachedCalledDuringMomentum = true;
  }

  componentWillMount() {
    this._loadData();
  }

  render() {
    if (!this.state.data.length) {
      return <ActivityIndicator style={{padding: 20}}/>;
    }
    let props = Object.assign({}, this.props, {
      debug: false,
      data: this.state.data,
      numColumns: 2,
      renderItem: this._itemRenderer.bind(this),
      keyExtractor: (item, index) => index,
      onEndReached: this._onEndHandler.bind(this),
      initialNumToRender: 2,
      removeClippedSubviews: true,
      onEndReachedThreshold: 0.5,
      onViewableItemsChanged: this._onViewableItemsChanged,
      onMomentumScrollBegin: () => {
        this.onEndReachedCalledDuringMomentum = false;
      },
      refreshControl: <RefreshControl
          colors={[COLORS.baseBG, COLORS.lighBG]}
          refreshing={this.state.refreshing}
          onRefresh={this._onRefreshHandler.bind(this)}
          tintColor={COLORS.lighBG}
          titleColor="#fff"
      />
    });
    return <FlatList {...props} />
  }
}