import React from 'react';
import {Header, Item} from 'native-base';


class HeaderComponent {
  _items = [];
  _opts = {};

  getComponent = (() => {
    let counter = 0;
    return () => {
      let items = null;
      if (this._items.length) {
        items = (<Item>
          {this._items}
        </Item>);
      }

      return (<Header {...this._opts}>
        {items}
      </Header>);
    }
  });

  appendItem = (item) => {
    this._items.push(item);
    return this;
  };

  setOption = (opt) => {
    Object.assign(this._opts, opt);
    return this;
  };
}

export const headerFactor = function () {
  return new HeaderComponent();
};
