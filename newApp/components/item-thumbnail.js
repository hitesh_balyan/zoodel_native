import React, {Component} from 'react';
import {Item, Text, View} from 'native-base';
import {Animated, Image, TouchableWithoutFeedback} from 'react-native';
import {COLORS} from "../constants";
import PropTypes from 'prop-types';


export class ItemThumbnail extends Component {
  static defaultProps = {
    uri: '',
    currency: '$',
    price: {
      current: 0,
      save: 0,
      isSaved: false,
      savePresent: 0
    },
    name: '--',
    sold: 0,
  };
  static propTypes = {
    uri: PropTypes.oneOfType([PropTypes.string]).isRequired,
    price: PropTypes.shape({
      current: PropTypes.number,
      original: PropTypes.number,
      isSaved: PropTypes.bool,
      savePresent: PropTypes.number,
    }),
    name: PropTypes.string,
    sold: PropTypes.number,
  };
  animationIn = () => {
    Animated.timing(this.state.press, {
      toValue: 0.8,
      duration: 200,
    }).start();
  };
  animationOut = () => {
    Animated.timing(this.state.press, {
      toValue: 1,
      duration: 200,
    }).start();
  };
  onPress = () => {
    if (this.props.onPress) {
      this.props.onPress();
    }
  };
  setVisibility = (visibility) => {
    this.setState({
      visible: visibility
    });
  };
  _soldTemplate = (data) => {
    if (!!data.sold) {
      return (<Text style={{
        position: 'absolute',
        bottom: 50,
        right: 10,
        backgroundColor: COLORS.gray,
        paddingHorizontal: 10,
        borderRadius: 10,
        fontSize: 12,
        color: COLORS.white,
        opacity: 0.7
      }}>
        sold {data.sold}
      </Text>)
    }
    return null;
  };
  _itemDetails = (data) => {
    if (data.price.isSaved) {
      return (
          <View style={{margin: 2}}>
            <Text style={{color: COLORS.gray, fontSize: 12, margin: 1}}>{data.name}</Text>

            <View style={{flexDirection: 'row', margin: 2, justifyContent: 'space-between'}}>
              <Text style={{
                fontSize: 12,
                fontWeight: 'bold',
              }}>{data.currency}{data.price.current}</Text>
              <Text style={{
                fontSize: 12,
                textDecorationLine: 'line-through',
                textDecorationStyle: 'solid',
              }}>{data.currency}{data.price.original}</Text>
            </View>
            <Text style={{
              fontSize: 12,
              margin: 1,
              color: 'blue'
            }}>Save {data.price.savePresent}%</Text>
          </View>
      )
    }
    return (
        <View style={{margin: 2}}>
          <Text style={{color: COLORS.gray, fontSize: 12, margin: 1}}>{data.name}</Text>
          <Text style={{
            fontSize: 12,
            fontWeight: 'bold',
            margin: 1
          }}>{data.currency}{data.price.current}</Text>

        </View>
    )
  };

  constructor(props) {
    super(props);
    this.state = {
      uri: this.props.uri,
      currency: this.props.currency,
      price: this.props.price,
      name: this.props.name.substr(0, 18),
      press: new Animated.Value(1),
      sold: this.props.sold,
      visible: true,
    };
    this.viewProperties = {
      width: 0,
      height: 0,
    };
  }

  onLayout(evt) {
    this.viewProperties.width = evt.nativeEvent.layout.width;
    this.viewProperties.height = evt.nativeEvent.layout.height;
  }

  render() {
    if (!this.state.visible) {
      return (<View style={{width: this.viewProperties.width, height: this.viewProperties.height}}/>)
    }

    let data = this.state;
    return (
        <TouchableWithoutFeedback
            onPressIn={() => (this.animationIn())}
            onPressOut={() => (this.animationOut())}
            onPress={this.onPress.bind(this)}
            onLayout={this.onLayout.bind(this)}
        >
          <Animated.View
              style={{
                flex: 1, margin: 5,
                height: 200, width: null,
                transform: [{scale: this.state.press}]
              }}>
            <Image style={{width: null, height: 180, flex: 1}} source={{uri: data.uri}}/>
            {this._soldTemplate(data)}
            {this._itemDetails(data)}
          </Animated.View>
        </TouchableWithoutFeedback>
    );
  }
}
