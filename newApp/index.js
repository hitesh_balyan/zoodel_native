"use strict";
import React from 'react';
import {StyleProvider} from 'native-base';
import Router from './routes';
import getTheme from './../native-base-theme/components';
import material from './../native-base-theme/variables/material';

export default () => {
  return (<StyleProvider style={getTheme(material)}>
    <Router/>
  </StyleProvider>)
};

