import {Dimensions} from 'react-native';

const {width, height} = Dimensions;
const COLORS = {
  baseBG: '#ffcd2c',
  lighBG: '#ffd44b',
  active: '#222222',
  inactive: '#afaeae',
  gray: '#616161',
  white: '#ffffff'
};

export {
  COLORS,
}