import React from 'react';
import {Body, Card, CardItem, Content, Header, Icon, List, ListItem, Separator, Text, Thumbnail, View} from 'native-base';
import { NavigationActions } from 'react-navigation'

let orderSettings = [
  {icon: 'ios-cube-outline', text: 'order', navigate: 'accountAllOrderTab'},
  {icon: 'ios-exit-outline', text: 'Unpaid', navigate: 'accountUnpaidOrderTab'},
  {icon: 'ios-jet-outline', text: 'Paid', navigate: 'accountPaidOrderTab'},
  {icon: 'ios-checkbox-outline', text: 'Finish', navigate: 'accountFinishOrderTab'},
];

let userSettingOptions = [
  {text: 'account account', navigate: 'accountUserAccount'},
  {text: 'my favorite', navigate: 'accountFavorite'},
  {text: 'app settings', navigate: 'accountAppSettings'},
  {text: 'contact zoodmall', navigate: 'accountContact'}
];

function navigateTo(route, address) {
  const navigateAction = NavigationActions.navigate({
    routeName: route,
    action: NavigationActions.navigate({ routeName: address})
  });

  this.navigation.dispatch(navigateAction)
}

function SettingProfileScreen(prpos) {
  let navigate = navigateTo.bind(prpos);

  return <Content>
    <Header style={{height: 150}}>
      <Body>
      <View style={{flexDirection: 'row', marginVertical: 10}}>
        <Thumbnail large size={250} style={{marginHorizontal: 20,}}
                   source={require('./../../res/3.jpg')}/>
        <View style={{flex: 1, flexDirection: 'column'}}>
          <Text style={{flex: 1}}>Username</Text>
          <Text style={{flex: 1}}>SubText</Text>
          <Text style={{flex: 1}}>Email</Text>
        </View>
      </View>
      <Text>Coupons(0)</Text>
      </Body>
    </Header>

    <View>
      <Card>
        <CardItem>
          <List dataArray={orderSettings}
                button={true}
                renderRow={(item) => (
                    <ListItem onPress={() => {
                      navigate('accountOrderPage', item.navigate)
                    }}>
                      <Icon style={{fontSize: 12}} name={item.icon}/>
                      <Text style={{marginHorizontal: 10}}>{item.text.toUpperCase()}</Text>
                    </ListItem>
                )}
          />
        </CardItem>
      </Card>

      <Card>
        <CardItem>
          <List dataArray={userSettingOptions}
                button={true}
                renderRow={(item) => (
                    <ListItem onPress={() => {
                      navigate('accountUserAccountPage', item.navigate)
                    }}>
                      <Text>{item.text.toUpperCase()}</Text>
                      <Icon style={{fontSize: 12, position: 'absolute', right: 0}} name="ios-arrow-forward"/>
                    </ListItem>
                )}
          />
        </CardItem>
      </Card>
    </View>
  </Content>
}


export default SettingProfileScreen;