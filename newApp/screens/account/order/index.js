import React from 'react';
import {TabNavigator} from "react-navigation";
import OtherScreen from "./other-screen";


export default TabNavigator({
  accountAllOrderTab: {
    screen: OtherScreen,
    navigationOptions:{
      title: 'All'
    }
  },
  accountUnpaidOrderTab: {
    screen: OtherScreen,
    navigationOptions:{
      title: 'Unpaid'
    }
  },
  accountPaidOrderTab: {
    screen: OtherScreen,
    navigationOptions:{
      title: 'Paid'
    }
  },
  accountFinishOrderTab: {
    screen: OtherScreen,
    navigationOptions:{
      title: 'Finish'
    }
  }
}, {
  backBehavior: 'none'
});