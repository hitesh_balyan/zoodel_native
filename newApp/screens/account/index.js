import {StackNavigator} from "react-navigation";
import SettingProfileScreen from "./profile";
import OrderScreen from "./order";
import UserAccountScreen from "./user-account/index";

export default StackNavigator({
  accountMainPage:{
    screen: SettingProfileScreen,
    navigationOptions: {header: null}
  },
  accountOrderPage:{
    screen: OrderScreen,
  },
  accountUserAccountPage:{
    screen: UserAccountScreen,
  }
},{
  initialRouteName:'accountMainPage'
});
