import React from 'react';
import {StackNavigator} from "react-navigation";
import OtherScreen from "./other-screen";


export default StackNavigator({
  accountUserAccount: {
    screen: OtherScreen,
    navigationOptions: {
      title: 'Account Setting'
    }
  },

  accountFavorite: {
    screen: OtherScreen,
    navigationOptions: {
      title: 'MY FAVORITE'
    }
  },

  accountAppSettings: {
    screen: OtherScreen,
    navigationOptions: {
      title: 'APP SETTINGS'
    }
  },

  accountContact: {
    screen: OtherScreen,
    navigationOptions: {
      title: 'CONTACT ZOODMALL'
    }
  },
}, {
  initialRouteName: 'accountUserAccount',
  headerMode:'none'
});