import React, {Component} from 'react';
import {Button, Content, Text} from 'native-base';

export default class OtherScreen extends Component {
  render() {
    return (
        <Content>
          <Button onPress={() => {
            this.props.navigation.navigate('home')
          }}>
            <Text>In Progress</Text>
          </Button>
        </Content>
    )
  }
}