import React, {Component} from 'react';
import {Content, Text, Button , Input,View,InputGroup} from 'native-base';
import {StyleSheet } from 'react-native';

export default class UserContactScreen extends Component {
    render() {
        return (
            <Content>
                <Input placeholder="FirstName" style={{ backgroundColor: '#aaaaaa', marginTop:10, marginBottom:10,marginLeft:10,marginRight:10,}}  />
                <Input placeholder="LastName"  style={{ backgroundColor: '#aaaaaa', marginTop:10, marginBottom:10,marginLeft:10,marginRight:10,}} />
              <View style={{flex: .1, flexDirection: 'row',marginTop:420}}>
                <View style={{flex: 1, backgroundColor: '#ffffff'}}>
                  <Text style={{textAlign: 'center', justifyContent: 'center', color: "#000000", paddingTop: 15,paddingBottom: 15}}>
                    Cancle
                  </Text>
                </View>
                <View style={{flex: 1, backgroundColor: '#000000'}}>
                  <Text style={{textAlign: 'center', justifyContent: 'center', color: "#ffffff", paddingTop: 15,paddingBottom: 15}}>
                    Submit
                  </Text>
                </View>
              </View>
            </Content>
        )
    }
}

