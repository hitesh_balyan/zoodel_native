import React, {Component} from 'react';
import {Content, Text, Button , Input,View} from 'native-base';

export default class UserPasswordScreen extends Component {
    render() {
        return (
            <Content>
                <Input placeholder="Old Password" secureTextEntry={true} style={{ backgroundColor: '#aaaaaa', marginTop:10, marginBottom:10,marginLeft:10,marginRight:10,}}  />
                <Input placeholder="New Password" secureTextEntry={true}  style={{ backgroundColor: '#aaaaaa', marginTop:10, marginBottom:10,marginLeft:10,marginRight:10,}} />
                <Input placeholder="Confirm Password" secureTextEntry={true}  style={{ backgroundColor: '#aaaaaa', marginTop:10, marginBottom:10,marginLeft:10,marginRight:10,}} />
              <View style={{flex: .1, flexDirection: 'row',marginTop:350}}>
                <View style={{flex: 1, backgroundColor: '#ffffff'}}>
                  <Text style={{textAlign: 'center', justifyContent: 'center', color: "#000000", paddingTop: 15,paddingBottom: 15}}>
                    Cancle
                  </Text>
                </View>
                <View style={{flex: 1, backgroundColor: '#000000'}}>
                  <Text style={{textAlign: 'center', justifyContent: 'center', color: "#ffffff", paddingTop: 15,paddingBottom: 15}}>
                    Submit
                  </Text>
                </View>
              </View>
            </Content>
        )
    }
}
