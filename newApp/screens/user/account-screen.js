import React, {Component} from 'react';
import {Content, Text, Button , View, Card, CardItem , ListItem, Icon, Header,Body,Thumbnail} from 'native-base';

export default class AccountScreen extends Component {
    render() {
        return (
            <Content>
              <Header style={{height: 60  }}>
                <Body>
                <View style={{flexDirection: 'row', marginVertical: 5}}>
                  <View style={{flex: 1}}>
                    <Text style={{flex: 1 ,marginLeft:20}}>My Photo</Text>
                  </View>
                  <Thumbnail small size={100} style={{marginHorizontal: 5,}}
                             source={require('./../../res/3.jpg')}/>
                </View>
                </Body>
              </Header>
              <View>

                <Card>
                  <CardItem>
                    <Content>
                      <ListItem onPress={() => (this.props.navigation.navigate('userContactScreen'))}>
                        <Text>{'Contact Name'.toUpperCase()}</Text>
                        <Icon style={{fontSize: 12, position: 'absolute', right: 0}} name="ios-arrow-forward"/>
                      </ListItem>
                      <ListItem onPress={() => (this.props.navigation.navigate('userPasswordScreen'))}>
                        <Text>{'Password'.toUpperCase()}</Text>
                        <Icon style={{fontSize: 12, position: 'absolute', right: 0}} name="ios-arrow-forward"/>
                      </ListItem>
                      <ListItem onPress={() => (this.props.navigation.navigate('userEmailScreen'))}>
                        <Text>{'Email'.toUpperCase()}</Text>
                        <Icon style={{fontSize: 12, position: 'absolute', right: 0}} name="ios-arrow-forward"/>
                      </ListItem>
                      <ListItem onPress={() => (this.props.navigation.navigate('userPhoneScreen'))}>
                        <Text>{'Mobile'.toUpperCase()}</Text>
                        <Icon style={{fontSize: 12, position: 'absolute', right: 0}} name="ios-arrow-forward"/>
                      </ListItem>
                      <ListItem onPress={() => (this.props.navigation.navigate('appSettingScreen'))}>
                        <Text>{'Shipping Address'.toUpperCase()}</Text>
                        <Icon style={{fontSize: 12, position: 'absolute', right: 0}} name="ios-arrow-forward"/>
                      </ListItem>
                    </Content>
                  </CardItem>
                </Card>
              </View>
            </Content>
        )
    }
}