import React from 'react';
import {Button, Header, Icon, Input, Item, ScrollableTab, Tab, Tabs, View} from 'native-base';


import {GetListBy} from './../api';
import {ItemList} from "./../components/item-list";
import {COLORS} from "../constants";


export default class MallScreen extends React.PureComponent {
  activeTab = 0;
  tabs = [
    {key: 'WOMEN\'S CLOTHING', data: {categoryIds: 1, categoryId: 1}, ref: null},
    {key: 'MEN\'S CLOTHING', data: {categoryIds: 2, categoryId: 2}, ref: null},
    {key: 'PHONES & ACCESSORIES', data: {categoryIds: 3, categoryId: 3}, ref: null},
    {key: 'COMPUTER & OFFICE', data: {categoryIds: 4, categoryId: 4}, ref: null},
    {key: 'CONSUMER ELECTRONICS', data: {categoryIds: 5, categoryId: 5}, ref: null},
    {key: 'JEWELRY & WATCHES', data: {categoryIds: 6, categoryId: 6}, ref: null},
    {key: 'HOME & GARDEN', data: {categoryIds: 7, categoryId: 7}, ref: null},
    {key: 'BAGS & SHOES', data: {categoryIds: 8, categoryId: 8}, ref: null},
    {key: 'TOYS,KIDS & BABY', data: {categoryIds: 9, categoryId: 9}, ref: null},
    {key: 'SPORTS & OUTDOORS', data: {categoryIds: 10, categoryId: 10}, ref: null},
    {key: 'HEALTH & BEAUTY', data: {categoryIds: 11, categoryId: 11}, ref: null},
    {key: 'AUTOMOBILES & MOTORCYCLES', data: {categoryIds: 12, categoryId: 12}, ref: null},
    {key: 'HOME IMPROVEMENT', data: {categoryIds: 13, categoryId: 13}, ref: null},
    {key: 'RECENTLY VIEWED', data: {categoryIds: 9999, categoryId: 9999}, ref: null},
  ];

  _addRef = (index, item) => {
    this.tabs[index].ref = item;
  };

  render() {
    return (
        <View style={{flex: 1}}>
          <Header hasTabs searchBar rounded>
            <Item>
              <Icon name='qr-scanner' style={{color: COLORS.white}}/>

              <Icon name="ios-search"/>
              <Input placeholder="Search"/>
              <Icon name="md-color-filter" style={{color: COLORS.white}}/>
            </Item>
          </Header>

          <Tabs renderTabBar={() => <ScrollableTab style={{height: 35, backgroundColor: '#ffffff'}}/>}
                locked={false}
                tabBarUnderlineStyle={{height: 1}}
                onChangeTab={(data) => {
                  let {i, ref, from} = data;
                  this.tabs[this.activeTab].ref.setVisibility(false);
                  this.activeTab = i;
                  this.tabs[this.activeTab].ref.setVisibility(true);
                }}>
            {this.tabs.map((k, index) => {
              return (
                  <Tab key={index} heading={k.key}>
                    <ItemList
                        navigation={this.props.navigation}
                        ref={item => this._addRef(index, item)}
                        name={k.key}
                        dataHandler={(cb, page) => {
                          GetListBy(cb, page, k.data)
                        }}/>
                  </Tab>
              )
            })}
          </Tabs>
        </View>
    )
  }
}