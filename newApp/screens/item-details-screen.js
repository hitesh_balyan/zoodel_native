import React, {Component} from 'react';
import {Card, CardItem, Content, Text} from 'native-base';
import {ActivityIndicator, Dimensions, Image, ScrollView, StyleSheet, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import {GetProductDetails} from './../api';

const {width, height} = Dimensions.get('window');
const halfHeight = width * 0.5;

export default class ItemDetailScreen extends Component {
  params = {};
  data = {};

  constructor(props) {
    super(props);
    this.data = props.navigation.state.params.data;
    this.params = props.navigation.state.params;
    this.state = {
      isLoading: true,
      images: [],
      data: {}
    }
  }

  componentWillMount() {
    let data = {
      productId: this.data.productId,
      customerId: this.data.customerId,
      sponsorShopId: this.data.sponsorShopId
    };
    GetProductDetails(data, (err, data) => {
      let images = [];
      for (let key in data) {
        if (key.substr(0, 6) === 'imgUrl' && data[key].length) {
          images.push(data[key]);
        }
      }

      this.setState({
        isLoading: false,
        images: images,
        data: data
      })
    });
  }

  render() {
    let data = this.state.data;
    if (Object.keys(data).length === 0)
      return <ActivityIndicator style={{padding: 20}}/>;
    let shipping = data.shipping[0];
    let specifics = data.specifics;
    return (
        <View style={{flex: 1, flexDirection: 'column'}}>
          <ScrollView style={{flex: .9}}>
            <Card>
              <CardItem>
                <ScrollView horizontal pagingEnabled showsHorizontalScrollIndicator={false}>
                  {this.state.images.map(
                      (image, i) => {
                        return (<Image key={i} style={styles.image} source={{uri: image}}/>)
                      }
                  )}
                </ScrollView>
              </CardItem>
              <CardItem>
                <Text style={{
                  textAlign: 'center',
                  fontSize: 12,
                  flex: 1,
                  flexDirection: 'row'
                }}>{data.name}</Text>
              </CardItem>
              <CardItem>
                <Text style={{textAlign: 'center', fontSize: 12, flex: 1, flexDirection: 'row'}}>
                  <Text>${data.defaultPrice} </Text>
                  <Text style={{textDecorationLine: 'line-through', textDecorationStyle: 'solid',}}>
                    ${data.defaultPrice}
                  </Text>
                </Text>
              </CardItem>
            </Card>

            <Card>
              <CardItem>
                <View style={{flex: 1, flexDirection: 'column'}}>
                  <View style={{
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    marginBottom: 6,
                    borderColor: '#aaa',
                    paddingBottom: 2,
                  }}>
                    <Text style={{flex: 9}}>Shipping and Payment</Text>
                    <Icon style={{flex: 1}} name="ios-arrow-forward"/>
                  </View>
                  <View style={{marginBottom: 6}}>
                    <Text style={{fontSize: 12}}>{shipping.shippingTypeTxt}</Text>
                  </View>
                  <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10}}>
                    <Text style={{fontSize: 12}}>Cost: {shipping.shippingAmount}</Text>
                    <Text
                        style={{fontSize: 12}}>{shipping.shippingTimeTxt}: {shipping.shippingNum}</Text>
                  </View>
                  <View style={{
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    marginBottom: 6,
                    borderColor: '#aaa',
                    paddingBottom: 2,
                    flex: 1,
                    justifyContent: 'space-between'
                  }}>
                    <View>
                      <Text>ZoodMall Guarantee</Text>
                      <Text style={{fontSize: 12}}>Get this item you ordered or get your money
                        back</Text>
                    </View>
                    <View style={{paddingTop: 5}}>
                      <Icon name="ios-arrow-forward"/>
                    </View>
                  </View>
                  <View style={{flexDirection: 'column'}}>
                    <Text style={{paddingBottom: 10}}>Payment</Text>
                    <View style={{flexDirection: 'row'}}>
                      <Icon name="logo-bitcoin" size={30} style={{margin: 5}}/>
                      <Icon name="ios-card" size={30} style={{margin: 5}}/>
                      <Icon name="ios-cash" size={30} style={{margin: 5}}/>
                    </View>
                  </View>
                </View>
              </CardItem>
            </Card>

            <Card>
              <CardItem>
                <View style={{flex: 1, flexDirection: 'column'}}>
                  <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    borderBottomWidth: 1,
                    marginBottom: 6,
                    borderColor: '#aaa',
                    paddingBottom: 2,
                  }}>
                    <Text style={{flex: 9}}>Item Specifics</Text>
                    <Icon style={{flex: 1}} name="ios-arrow-forward"/>
                  </View>

                  <View style={{flex: 1, flexDirection: "column"}}>
                    {specifics.map((prop, key) => {
                      return (
                          <View style={{
                            flexDirection: 'row',
                            marginBottom: 5
                          }} key={key}>
                            <Text style={{flex: 1, fontSize: 12}}>{prop.name}</Text>
                            <Text style={{flex: 1, fontSize: 12}}>{prop.value}</Text>
                          </View>
                      )
                    })}
                  </View>
                </View>
              </CardItem>
            </Card>
          </ScrollView>
          <View style={{flex: .1, flexDirection: 'row'}}>
            <View style={{flex: 1, backgroundColor: '#ffffff'}}>
              <View style={{flexDirection: 'row', padding: 10, justifyContent: 'space-between'}}>
                <Icon size={30} name="ios-share-alt-outline"/>
                <Icon size={30} name="ios-heart-outline"/>
                <Icon size={30} name="ios-cart-outline"/>
              </View>
            </View>
            <View style={{flex: 1, backgroundColor: '#000000'}}>
              <Text style={{textAlign: 'center', justifyContent: 'center', color: "#ffffff", paddingTop: 15}}>
                Add To Cart
              </Text>
            </View>
          </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  scrollContainer: {
    height: halfHeight,
  },
  image: {
    width,
    height: halfHeight,
  },
});