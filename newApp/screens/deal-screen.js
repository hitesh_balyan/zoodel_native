import React, {Component} from 'react';
import {Tab, Tabs, View} from 'native-base';
import {ItemList} from "../components/item-list";
import {GetDeals} from './../api';
import Container from '../store';


export default class DealScreen extends Component {
  activeTab = 0;
  tabs = [
    {key: 'Previous Deals', data: {type: 1}, ref: null},
    {key: 'OnSale New', data: {type: 2}, ref: null},
    {key: 'Upcoming Deals', data: {type: 3}, ref: null},
  ];

  _addRef = (index, item) => {
    this.tabs[index].ref = item;
  };

  render() {
    console.log(Container.get());
    return (
        <View style={{flex: 1}}>
          <Tabs initialPage={1}>
            {this.tabs.map((k, index) => {
              return (
                  <Tab key={index} heading={k.key}>
                    <ItemList
                        navigation={this.props.navigation}
                        ref={item => this._addRef(index, item)}
                        name={k.key}
                        dataHandler={(cb, page) => {
                          GetDeals(cb, page, k.data)
                        }}/>
                  </Tab>
              )
            })}
          </Tabs>
        </View>
    );
  }
}