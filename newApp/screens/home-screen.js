import React from 'react';
import {Button, Icon, Input, Item, ScrollableTab, Tab, Tabs, View} from 'native-base';
import _ from 'lodash';


import {headerFactor} from './../components/header';
import {GetListBy} from './../api';
import {ItemList} from "./../components/item-list";
import {COLORS} from "../constants";


export default class HomeScreen extends React.PureComponent {
  activeTab = 0;
  tabs = [
    {key: 'POPULAR', data: {isPopular: true}, ref: null},
    {key: 'RECOMMENDED', data: {isRecommended: true}, ref: null},
    {key: 'SALE', data: {isSale: true}, ref: null},
  ];
  _header = null;

  _addRef = (index, item) => {
    this.tabs[index].ref = item;
  };

  componentWillMount() {
    this._header = headerFactor()
        .appendItem(<Icon key={_.uniqueId()} name='qr-scanner' style={{color: COLORS.white}}/>)
        .appendItem(<Input key={_.uniqueId()} placeholder="Search"/>)
        .appendItem(<Icon key={_.uniqueId()} name="ios-search"/>)
        .setOption({hasTabs: true, searchBar: true, rounded: true})
        .getComponent();
  }

  render() {
    let Header = this._header;
    return (
        <View style={{flex: 1}}>

          <Header/>
          <Tabs renderTabBar={() => <ScrollableTab style={{height: 35, backgroundColor: '#ffffff'}}/>}
                locked={false}
                tabBarUnderlineStyle={{height: 1}}
                onChangeTab={(data) => {
                  let {i, ref, from} = data;
                  if (this.tabs[i] !== void 0) {
                    this.tabs[this.activeTab].ref.setVisibility(false);
                    this.activeTab = i;
                    this.tabs[this.activeTab].ref.setVisibility(true);
                  } else {
                    //@todo hardcoded need to change
                    this.props.navigation.navigate('mallMain');
                  }
                }}>
            {this.tabs.map((k, index) => {
              return (
                  <Tab key={index} heading={k.key}>
                    <ItemList
                        navigation={this.props.navigation}
                        ref={item => this._addRef(index, item)}
                        name={k.key}
                        dataHandler={(cb, page) => {
                          GetListBy(cb, page, k.data)
                        }}/>
                  </Tab>
              )
            })}
            <Tab heading="MORE..."></Tab>
          </Tabs>
        </View>
    )
  }
}
