/**
 * Created by hitesh on 30-12-2017.
 */

import React from 'react';
import {Body, Button, Card, CardItem, Header, Icon, Input, List, ListItem, Right, SwipeRow, Text, View} from 'native-base';
import {ItemList} from "./../components/item-list";
import {COLORS} from "../constants";
import {GetListBy} from "../api";

export default class CartScreen extends React.Component {

  render() {
    return (
        <View style={{flex: 1}}>
          <ItemList
              ListHeaderComponent={<View>
                <Header>
                  <Body style={{alignItems: 'center'}}>
                  <Text>CART({1})</Text>
                  </Body>
                </Header>
                <Card>
                  <CardItem header style={{borderBottomColor: COLORS.gray}}>
                    <Text>Best Deals</Text>
                  </CardItem>
                  <CardItem>
                    <SwipeRow rightOpenValue={-75}
                              body={<View style={{flexDirection: 'row'}}>
                                <View style={{flex: .3}}>
                                  <View style={{
                                    width: 85,
                                    height: 85,
                                    borderWidth: 0.5,
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                  }}>
                                    <Text>Image</Text>
                                  </View>
                                </View>
                                <View style={{flex: .7, flexDirection: 'column'}}>
                                  <Text>Item Item
                                    ItemItemItemItemItemItemItemItemItemItemItemItemItemItem</Text>
                                  <Text>$1500</Text>
                                  <View style={{flexDirection: 'row'}}
                                  >
                                    <Button transparent rounded bordered small><Text>-</Text></Button>
                                    <Input value={'1'}/>
                                    <Button transparent rounded bordered small><Text>+</Text></Button>
                                  </View>
                                  <Text>Details</Text>
                                </View>
                              </View>}
                              right={
                                <Button danger onPress={() => alert('Trash')}>
                                  <Icon active name="trash"/>
                                </Button>
                              }
                    />
                  </CardItem>
                </Card>
              </View>}
              navigation={this.props.navigation}
              dataHandler={(cb, page) => {
                GetListBy(cb, page, {isPopular: true})
              }}/>
        </View>
    )
  }
}