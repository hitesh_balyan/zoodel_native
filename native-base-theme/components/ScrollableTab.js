import variable from './../variables/platform';

export default (variables = variable) => {
  const ScrollableTab = {
    '.tab': {
      height: 15,
      alignItems: "center",
      justifyContent: "center",
      paddingLeft: 20,
      paddingRight: 20,
    },
    '.container': {
      height: 15,
      borderWidth: 1,
      borderTopWidth: 0,
      borderLeftWidth: 0,
      borderRightWidth: 0,
      borderColor: "#ccc",
    },
    '.tabs': {
      flexDirection: "row",
      justifyContent: "space-around",
    },
  };

  return ScrollableTab;
};
